# Administrarea unei sali de fitness



Proiectul isi propune furnizarea si implementarea serviciior specifice unei sali de fitness administrata online. Din punct de vedere al clasei de utilizatori carora li se adreseaza, serviciile se impart in trei categorii: servicii furnizate admninistratorului, servicii furnizate antrenorilor si servicii furnizate clientilor.

### Cerinte functionale
Acest proiect trebuie ca la final sa indeplineasca urmatoarele cerinte functionale:
- CRUD pe datele persistente ale bazei de date
-	Autorizarea accesului in functie de rolul, respectiv drepturile utilizatorului
-	Logarea utilizatorului
-	Existenta unei baze de date
-	Notificarea clientilor in momentul modificarii unei clase la care sunt abonati
-	Existenta unui orar saptamanal de ore de fitness si posibilitatea vizualizarii numarului de locuri ocupate/libere in cadrul fiecaruia
-	Confirmarea utilizatorului privind abonarea si dezabonarea acestuia in cadrul unei ore de fitness

### Cerinte de proiectare
Ca cerinte de proiectare, intalnim:
- 	Principiile SOLID sa fie indeplinite
-	Utilizarea pattern-ului Singleton in crearea bazei de date
-	Utilizarea pattern-ului Observer
-	Utilizarea pattern-ului Factory

### Rolurile utilizatorilor
Fiecare utilizator are unul sau mai multe roluri. In functie de rolul desemnat, acesta beneficiaza de anumite privilegii. Acestea din urma furnizeaza utilizatorului accesul la anumite operatii, servicii ale aplicatiei. Cu cat rolul unui utilizator este mai mare, cu atat acesta poate manipula mai multe informatii si accesa un numar mai mare de resurse.

Rolurile sunt dupa cum urmeaza
-   ADMIN
-   TRAINER
-   CLIENT

Un rol poate avea unul sau mai multe privilegii, iar un anumit privilegiu poate sa apare ca drept pentru mai multe roluri


### Clase
Din analiza cerintelor proiectului reies anumite entitati necesare in dezvoltarea acestuia. 

In primul rand este necesara clasa **User** pentru a reprezinta entitatea utilizatorului care acceseaza serviciile aplicatiei. Acesta incorporeaza anumite informatii necesare identificarii unui utilizator, cum ar fi: username, password, name.

Clasa **FClass** reprezinta informatiile necesare evidentierii unei clase de fitness (name, strength, styleID).

Clasa **Role** incorporeaza informatii despre rolurile si drepturile unui utilizator.

Clasa **Style** este creata pentru a ilustra tipurile de clase de fitness pe care aplicatia le va contine.
Clasa **Notification** are rolul de a reprezenta mesajele impreuna cu data la care a fost emise catre clienti, in momentul modificarii unei clase la care acestia sunt abonati

Clasa **TimetableClass** apare ca o necesitate in implementarea sablonului de proiectare Abstraction Occurence. Avand in vedere ca o clasa poate aparea in orar de mai multe ori, in mod cert cu aceleasi informatii de baza, cu exceptia datei si orei, avem nevoie de o clasa care sa reprezinte aparitiile unei clase de fitness in programul salii de fitness.

### Baza de date
Analizand cerintele aplicatiei, dar si informatiile ilustrate mai sus, se contureaza o idee generala despre cum structura bazei de date va arata.

Relatiile dintre tabele sunt alese dupa un rationament ce tine in mare parte de analiza cerintelor.

Intre tabelele User si FClass se va contura o relatie Many to Many deoarece un utilizator se poate abona la mai multe clase, iar la o clasa pot fi abonati mai multi utilizatori.

Intre Role si Privilege avem tot o relatie Many to Many deoarece un rol poate avea mai multe drepturi, iar un privilegiu poate poate fi atribuit mai multor roluri.

Intre User si Role avem o relatie Many to Many deoarece un utilizator poate avea mai multe roluri, iar un rol poate fi atribuit mai multor utilizatori.

Intre FClass si Style avem o relatie Many to One deoarece un clasa poate avea un singur stil, iar un stil de fitness poate fi atribuit mai multor clase.

**Diagrama care ilustreaza structura bazei de date poate fi accesata  [AICI](https://ibb.co/d2cJgpF).
**Diagrama baze de date actualizate : [AICI](https://ibb.co/5MJBFT7)
**Diagrama baze de date finala : [AICI](https://ibb.co/6bg1yxy)
**Diagrama UML de clase [AICI](https://ibb.co/J5RSQQ8)
**Diagrama UML de clase finala  [AICI](https://ibb.co/3r0s7bY)

### Organizarea in pachete
Pentru o gestionare mai buna a claselor ce vor constitui baza aplicatiei, este necesara organizarea in pachete. Se evidentiaza 6 pachete de baza:
-   **models** - contine clasele model (bean-urile)
-   **controllers** - contine controllerele folosite (ex. AdminController, TrainerController)
-   **service** - contine clasele care se ocupa cu logica aplicatiei
-   **data** - contine clasele DAO, care acceseaza in mod direct baza de date
-   **test** - contine clasele de test necesare de-a lungul dezvoltarii aplicatiei, pentru validarea nivelului de implementare
-   **configuration** - contine clasele necesare configurarii conexiunii cu baza de date, dar si un subpachet **handlers** in care se afla clasele care se ocupa cu accesarea unei pagini web in functie de rolul utilizatorului in cadrul aplicatiei

**Diagrama uml care ilustreaza legaturile dintre pachete poate fi accesata [aici](https://ibb.co/Bzgq5sQ)

### Organizarea in layere
Deoarece avem nevoie de date persistente, o decizie in cadrul dezvoltarii aplicatiei este de a organiza clasele pe layere (**Three-tier architecture**). 
-   in layer se va ocupa de operatiile cu baza de date (create, update, delete) - clasele DAO
-   un layer se va ocupa de logica aplicatiei - pachetul Service
-   un layer se va ocupa de furnizarea unformatiilor catre utilizator 

### Functionalitatea

In functie de rolul utilizatorului, acesta are posibilitatea de a accesa anumite operatii. Principalele operatii sunt:
-   Vizualizarea orarului cu clasele de fitness - se va folosi o metoda care va furniza toate claele stocate in baza de date care se vor desfasura in saptamana curenta
-   Crearea unui cont - datele furnizate de catre utilizator vor fi preluate si validate (username unic, parola constransa de lungime, caractere). Se va crea un nou user care va fi adaugat in baza de date - **Salting** pentru stocarea parolei
-   Abonarea la o clasa - deoarece in baza de date intre User si FClass avem o relatie many-to-many este necesara cunoasterea celor 2 chei primare si adaugarea perechii formate din acestea in tabelul care se ocupa cu gestiunea acestei relatii
- Dezabonarea de la o clasa - similar ca mai sus doar ca necesita stergerea tuplei formata din cele 2 chei  

### Design patternuri
Deoarece una dintre cerinte este notificarea antrenorilor in momentul abonarii/dezabonarii clientului la una din orele pe care acestia le presteaza o decizie privin utilizarea sabloanelor de proiectare ar fi folosirea patternului Observer. In momentul in care o persoana apasa butonul de abonare sau dezabonare, unul din observeri menit sa se ocupe cu aceasta parte se sesizeaza privind operatia aleasa.
Utilizarea unei baze de date la nivelul aplicatiei indica oarecum necesitatea unei instante unice a conexiunii in ceea ce o priveste. Pentru a ne asigura ca lucram cu o singura baza de date si ca avem o conexiune unica la nivel de aplicatie, vom volosi design patternul Singleton,

#### Singleton
Pattern-ul Singleton este utilizat pentru a restricționa numărul de instanțieri ale unei clase la un singur obiect, deci reprezintă o metodă de a folosi o singură instanță a unui obiect în aplicație. Aplicarea pattern-ului Singleton constă în implementarea unei metode ce permite crearea unei noi instanțe a clasei dacă aceasta nu există, și întoarcerea unei referințe către aceasta dacă există deja. În Java, pentru a asigura o singură instanțiere a clasei, constructorul trebuie să fie private, iar instanța să fie oferită printr-o metodă statică, publică.

În cazul unei implementări Singleton, clasa respectivă va fi instanțiată lazy (lazy instantiation), utilizând memoria doar în momentul în care acest lucru este necesar deoarece instanța se creează atunci când se apelează getInstance(), acest lucru putând fi un avantaj în unele cazuri, față de clasele non-singleton, pentru care se face eager instantiation, deci se alocă memorie încă de la început, chiar dacă instanța nu va fi folosită

Singleton este utilizat des în situații în care avem obiecte care trebuie accesate din mai multe locuri ale aplicației:

-   obiecte de tip logger
-   obiecte care reprezintă resurse partajate (conexiuni, sockets etc.)
-   obiecte ce conțin configurații pentru aplicație
-   pentru obiecte de tip Factory.

#### Factory
Folosind pattern-ul Factory Method se poate defini o interfață pentru crearea unui obiect. Clientul care apelează metoda factory nu știe/nu îl interesează de ce subtip va fi la runtime instanța primită.

Spre deosebire de Abstract Factory, Factory Method ascunde construcția unui obiect, nu a unei familii de obiecte “inrudite”, care extind un anumit tip. Clasele care implementează Abstract Factory conțin de obicei mai multe metode factory.

#### Observer
Design Pattern-ul Observer definește o relație de dependență 1 la n între obiecte astfel încât când un obiect își schimbă starea, toți dependenții lui sunt notificați și actualizați automat. Folosirea acestui pattern implică existența unui obiect cu rolul de subiect, care are asociată o listă de obiecte dependente, cu rolul de observatori, pe care le apelează automat de fiecare dată când se întâmplă o acțiune.

Acest pattern este de tip Behavioral (comportamental), deorece facilitează o organizare mai bună a comunicației dintre clase în funcție de rolurile/comportamentul acestora. Observer se folosește în cazul în care mai multe clase(observatori) depind de comportamentul unei alte clase(subiect).

Pentru aplicarea acestui pattern, clasele aplicației trebuie să fie structurate după anumite roluri, și în funcție de acestea se stabilește comunicarea dintre ele. În exemplul din figure 4, avem două tipuri de componente, Subiect și Observator, iar Observator poate fi o interfață sau o clasă abstractă ce este extinsă cu diverse implementări, pentru fiecare tip de monitorizare asupra obiectelor Subiect.

#### Abstraction occurence
De multe intalnim cazul in care avem un set de obiecte inrudite, numite aparitii. Acestea impartasesc informatii comune, dar totusi difera unele de altele in moduri destul de importante. Acest design pattern se foloseste atunci cand vrem sa reprezentam aceste aparitii fara a duplica informatia ce le este comuna tuturor. 
In cazul proiectului acesta, clasa FClass contine informatia care apare in mod comun aparitiilor TimetableClass de pe orar. Fiecare aparitie TimetableClass mentine id-ul unui obiect FClass pe care il reprezinta in orarul salii de fitness. Astfel informatia nu se duplica si avem parte de o reprezentare mai usor de inteles si modelat in cadrul proiectului.

### Tipurile de utilizatori si functionalitatile
In functie de tipul de utilizator, acesta va fi directionat dupa logare pe o anumita pagina de Home, in functie de rolul sau.
Un **client** poate sa:
-   vizualizeze toate clasele de fitness 
-   vizualizeze orarul
-   vizualizeze toti trainerii
-   vada clasele la care s-a abonat
-   se aboneze la o clasa de fitness
-   se dezaboneze de la o clasa de fitness
-   isi vada notificarile cu privire la o eventuala modificare a unei clase la care este abonat

Un **trainer** poate sa:
-   vizualizeze toate clasele de fitness 
-   vizualizeze orarul
-   vizualizeze toti clientii care s-au abonat la clasele la care preda
-   vizualizeze toti clientii care s-au abonat la o anumita clasa la care preda
-   vizualizeze clasele la care preda
-   adauge o clasa pe orar
-   stearga o clasa de pe orar
-   stearga un client de la o clasa la care preda

**Adminul** poate sa:
-   Vizualizeze, stearga, adauge, modifice utilizatorii
-   Vizualizeze, stearga, adauge, modifice clase de fitness
-   Vizualizeze, stearga, adauge, modifice stilurile de fitness


### Autentificarea
Spring Security are o serie de filtre servlet. Cand o solicitare ajunge la server, aceasta este interceptata de aceasta serie de filtre. Filtrele sunt parcurse pana cand se ajunge la filtrul de autentificare. Atunci se extrag acreditarile furnizate - un nume de utilizator si o parola de la apelant.
Folosind valorile furnizate, filtrul UsernamePasswordAuthenticationFilter creeaza un obiect Authentication.In diagrama anterioara, UserPasswordAuthenticationToken este creat cu numele de utilizator și parola furnizate. Obiectul Authentication creat este apoi folosit pentru a apela metoda de autentificare în interfața AuthenticationManager: Implementarea este furnizata de ProviderManager, care are o lista cu configurarile.
Solicitarea trece prin mai multi provideri si incearca sa autentifice cererea. AuthenticationProvider necesita detalii despre utilizator, care sunt furnizate prin UserDetailsService.
Dacă totul merge bine, Spring Security creează un obiect de autentificare complet populat (autentificat: adevărat, lista de autorizare acordată și numele de utilizator), care va conține diferite detalii necesare. Obiectul Authentication este stocat în obiectul SecurityContext.


