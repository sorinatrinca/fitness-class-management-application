package com.utcn.ps2019.sampleproject.controllers;


import com.utcn.ps2019.sampleproject.data.*;
import com.utcn.ps2019.sampleproject.models.*;
import com.utcn.ps2019.sampleproject.service.FClassService;
import com.utcn.ps2019.sampleproject.service.RoleService;
import com.utcn.ps2019.sampleproject.service.TimetableClassService;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/admin")
@Controller
@EnableAutoConfiguration
public class AdminController {
    @Autowired
    private FClassService fClassService;

    @Autowired
    private UserService userService;

    @Autowired
    private StyleDao styleDao;

    @Autowired
    private FClassDao fClassDao;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private TimetableClassService timetableClassService;

    @Autowired
    private TimetableClassDao timetableClassDao;


    @RequestMapping(value = "homepage")
    public String homepage() {
        return "admin/homepage";
    }

    /**
     *
     * @return toti utilizatorii din baza de date
     */
    @RequestMapping(value = "allUsers")
    public String allUsers(Model model) {
        Iterable<User> users = userDao.findAll();

        model.addAttribute("users", users);
        model.addAttribute("title","All users");
        return "admin/allUsers";
    }

    /**
     *
     * @return toti trainerii din baza de date
     */
    @RequestMapping(value = "allTrainers")
    public String allTrainers(Model model) {
        Iterable<User> users = userService.findAllTrainers();

        model.addAttribute("users", users);
        model.addAttribute("title","All trainers");
        return "admin/allTrainers";
    }

    /**
     *
     * @return toti clientii din baza de date
     */
    @RequestMapping(value = "allClients")
    public String allClients(Model model) {
        Iterable<User> users = userService.findAllClients();

        model.addAttribute("users", users);
        model.addAttribute("title","All clients");
        return "admin/allClients";
    }

    /**
     *
     * @return toti utilizatorii din baza de date
     */
    @RequestMapping(value = "removeUser", method = RequestMethod.GET)
    public String removeUser(Model model, Authentication authentication) {
        Iterable<User> users = userDao.findAll();

        model.addAttribute("users", users);

        return "admin/removeUser";
    }

    /**
     *
     * Sterge utilizatorul cu id-ul id din baza de date
     */
    @RequestMapping(value = "removeUser", method = RequestMethod.POST)
    public String processUserRemove(@RequestParam int id, Model model) {
        User user = userDao.findById(id).orElse(new User());
        userDao.delete(user);
        return "redirect:/admin/removeUser";
    }

    /**
     *
     * @return o lista cu rolurile si o lista cu toti utilizatorii din baza de date
     */
    @RequestMapping(value = "updateUser", method = RequestMethod.GET)
    public String updateUser(Model model) {
        model.addAttribute("title","Update");
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findAllButAdmin();
        model.addAttribute("roles", my_roles);
        Iterable<User> clients = userDao.findAll();
        model.addAttribute("clients", clients);
        return "admin/updateUser";
    }

    /**
     *
     * Modifica utilizatorul cu id-ul userId, atribuindu-i totodata rolul cu id-ul roleId
     */
    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public String updateUserPP(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, @RequestParam int userId, Model model){

        if (errors.hasErrors()){
            model.addAttribute("title","Update");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            Iterable<User> clients = userDao.findAll();
            model.addAttribute("clients", clients);
            return "admin/updateUser";
        }

        User u = userDao.findById(userId).orElse(new User());
        u.setUsername(user.getUsername());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setEmail(user.getEmail());
        u.setPassword(user.getPassword());

        userService.addUser(u,roleId);

        userDao.save(u);
        return "admin/updateUser";
    }

    /**
     *
     * @return toate clasele de fitness existente in cadrul acestei sali de fitness
     */
    @RequestMapping(value = "allClasses")
    public String allClasses(Model model) {
        Iterable<FClass> classes = fClassDao.findAll();

        model.addAttribute("classes", classes);
        model.addAttribute("title","All classes");
        return "admin/allClasses";
    }

    /**
     *
     * @return toate stilurile claselor de fitness
     */
    @RequestMapping(value = "allStyles")
    public String allStyles(Model model) {
        Iterable<Style> styles = styleDao.findAll();

        model.addAttribute("styles", styles);
        model.addAttribute("title","All styles");
        return "admin/allStyles";
    }

    /**
     *
     * @return toate clasele de fitness din orar
     */
    @RequestMapping(value = "allTimetableClasses")
    public String allTimetableClasses(Model model) {
        Iterable<TimetableClass> tclasses = timetableClassDao.findAll();

        model.addAttribute("tclasses", tclasses);
        model.addAttribute("title","All timetable classes");
        return "admin/allTimetableClasses";
    }

    @RequestMapping(value = "removeTC", method = RequestMethod.GET)
    public String removeTC(Model model, Authentication authentication) {
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        model.addAttribute("title","Remove TC");
        return "admin/removeTC";
    }

    /**
     * Sterge o clasa de pe orar, clasa cu id-ul id
     */
    @RequestMapping(value = "removeTC", method = RequestMethod.POST)
    public String processRemoveTC(@RequestParam int id, Model model) {
        TimetableClass tc = timetableClassDao.findById(id).orElse(new TimetableClass());
        timetableClassDao.delete(tc);
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        return "redirect:/admin/removeTC";
    }

    @RequestMapping(value = "addTC", method = RequestMethod.GET)
    public String addTC(Model model) {
        model.addAttribute("title","Add TC");
        model.addAttribute("timetableClass",new TimetableClass());

        Iterable<FClass> classes = fClassDao.findAll();
        model.addAttribute("classes", classes);
        Iterable<User> trainers = userService.findAllTrainers();
        model.addAttribute("trainers", trainers);

        return "/admin/addTC";
    }

    /**
     * Adauga o clasa de fitness pe orar, care reprezinta clasa de fitness de baza cu id-ul classId
     */
    @RequestMapping(value = "addTC", method = RequestMethod.POST)
    public String processaddTC(@ModelAttribute @Valid TimetableClass timetableClass, Errors errors, @RequestParam int classId,@RequestParam int trainerId, Model model, Authentication authentication){
        User u = userDao.findById(trainerId).orElse(new User());
        timetableClassService.add(timetableClass, classId, u);
        model.addAttribute("message", "SUCCESSFUL REGISTRATION!");

        return "/admin/addTC";
    }

    @RequestMapping(value = "addStyle", method = RequestMethod.GET)
    public String addStyle(Model model) {
        model.addAttribute("title","Add style");
        model.addAttribute("style",new Style());


        return "/admin/addStyle";
    }

    /**
     * Adauga un nou stil de clasa de fitness
     */
    @RequestMapping(value = "addStyle", method = RequestMethod.POST)
    public String addStylepp(@ModelAttribute @Valid Style style, Errors errors, Model model){
        styleDao.save(style);
        model.addAttribute("message", "SUCCESSFUL ADDED!");

        return "/admin/addStyle";
    }

    @RequestMapping(value = "removeStyle", method = RequestMethod.GET)
    public String removeStyle(Model model) {
        Iterable<Style> styles = styleDao.findAll();

        model.addAttribute("styles", styles);
        model.addAttribute("title","Remove style");
        return "admin/removeStyle";
    }

    /**
     * Sterge un stil cu id-ul id, selectat de utilizator
     */
    @RequestMapping(value = "removeStyle", method = RequestMethod.POST)
    public String removeStyle(@RequestParam int id, Model model) {
        Style style = styleDao.findById(id).orElse(new Style());
        styleDao.delete(style);
        Iterable<Style> styles = styleDao.findAll();

        model.addAttribute("styles", styles);
        return "redirect:/admin/removeStyle";
    }

    @RequestMapping(value = "classClients", method = RequestMethod.GET)
    public String classClients(Model model) {
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        model.addAttribute("title","Class clients");
        return "admin/classClients";
    }

    /**
     * Returneaza toti clientii abonati la clasa de fitness de pe orar, cu id-ul classId
     */
    @RequestMapping(value = "classClients", method = RequestMethod.POST)
    public String classClients(@RequestParam int classId, Model model) {
        TimetableClass tc = timetableClassDao.findById(classId).orElse(new TimetableClass());
        List<User> users = tc.getUsers();
        List<User> toRemove = new ArrayList<>();
        if (users!=null) {
            for (User u : users) {
                if ("ROLE_TRAINER".equals(u.getRole().getName()))
                    toRemove.add(u);
            }
            users.removeAll(toRemove);
        }

        model.addAttribute("users", users);
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        return "admin/classClients";
    }

    @RequestMapping(value = "clientClasses", method = RequestMethod.GET)
    public String clientClasses(Model model) {
        Iterable<User> users = userDao.findAll();

        model.addAttribute("users", users);
        model.addAttribute("title","clients");
        return "admin/clientClasses";
    }

    /**
     * Returneaza toate clasele la care clientul cu id-ul clientId este abonat
     */
    @RequestMapping(value = "clientClasses", method = RequestMethod.POST)
    public String clientClasses(@RequestParam int clientId, Model model) {
        User user = userDao.findById(clientId).orElse(new User());

        List<TimetableClass> tcs = user.getTimetableClasses();
        Iterable<User> users = userDao.findAll();

        model.addAttribute("users", users);
        model.addAttribute("tcs", tcs);
        return "admin/clientClasses";
    }


    @RequestMapping(value = "changeTCdate", method = RequestMethod.GET)
    public String changeTCdate(Model model) {
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        model.addAttribute("title","clients");
        return "admin/changeTCdate";
    }

    /**
     * Modifica data desfasurarii unei clase de pe orar cu id-ul classId, atribuindu-i data si ora localDateTime
     */
    @RequestMapping(value = "changeTCdate", method = RequestMethod.POST)
    public String changeTCdatePP(@RequestParam int classId, @RequestParam String localDateTime, Model model) {
        TimetableClass timetableClass = timetableClassDao.findById(classId).orElse(new TimetableClass());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(localDateTime, formatter);

        timetableClass.setDate(dateTime);
        Iterable<User> users = timetableClass.getUsers();
        Notification n = new Notification();
        n.setMsg("The timetable has changed! Please take a look :)    [" + LocalDate.now().toString()+"]");
        notificationDao.save(n);

        for (User u: users){
           List<Notification> notificationList = u.getNotifications();
           if (notificationList==null)
               notificationList = new ArrayList<>();
           notificationList.add(n);

            User myUser = userDao.findByUsername(u.getUsername());
            myUser.setNotification(notificationList);
            userDao.save(myUser);
        }


        Iterable<TimetableClass> tcs = timetableClassDao.findAll();
        model.addAttribute("tcs", tcs);
        return "admin/changeTCdate";
    }

    @RequestMapping(value = "addFClass", method = RequestMethod.GET)
    public String addFCLASS(Model model) {

        model.addAttribute("fClass",new FClass());

        Iterable<Style> styles = styleDao.findAll();
        model.addAttribute("styles", styles);

        return "/admin/addFClass";
    }

    /**
     * Adauga o clasa de fitness pe orar, care reprezinta clasa de fitness de baza cu id-ul classId
     */
    @RequestMapping(value = "addFClass", method = RequestMethod.POST)
    public String addFCLASSPP(@ModelAttribute @Valid FClass fClass,@RequestParam int styleId, Errors errors, Model model){
        fClassDao.save(fClass);
        model.addAttribute("message", "SUCCESSFUL REGISTRATION!");

        return "/admin/addFClass";
    }

}
