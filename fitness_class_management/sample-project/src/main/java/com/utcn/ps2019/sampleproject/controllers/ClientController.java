package com.utcn.ps2019.sampleproject.controllers;

import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.Notification;
import com.utcn.ps2019.sampleproject.models.TimetableClass;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.service.FClassService;
import com.utcn.ps2019.sampleproject.service.TimetableClassService;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@RequestMapping("/client")
@Controller
@EnableAutoConfiguration
public class ClientController {
    @Autowired
    private FClassService fClassService;

    @Autowired
    private UserService userService;

    @Autowired
    private FClassDao fClassDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TimetableClassService timetableClassService;

    @Autowired
    private TimetableClassDao timetableClassDao;



    @RequestMapping(value = "homepage")
    public String homepage() {
        return "client/homepage";
    }


    /**
     *
     * @return toate clasele de fitness din baza de date
     */
    @RequestMapping(value = "allClasses")
    public String allClasses(Model model) {
        Iterable<FClass> classes = fClassDao.findAll();

        model.addAttribute("classes", classes);
        model.addAttribute("title","All classes");
        return "client/allClasses";
    }


    /**
     * Returneaza toate notificarile pe care le are un client
     */
    @RequestMapping(value = "notification")
    public String notification(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            List<Notification> notifications = user.getNotifications();
            model.addAttribute("notifications", notifications);
        }

        return "client/notification";
    }


    /**
     *
     * @return toate clasele de fitness de pe orar
     */
    @RequestMapping(value = "allTimetableClasses")
    public String allTimetableClasses(Model model) {
        Iterable<TimetableClass> tclasses = timetableClassDao.findAll();

        model.addAttribute("tclasses", tclasses);
        model.addAttribute("title","All timetable classes");
        return "client/allTimetableClasses";
    }


    /**
     *
     * @return toti trainerii din baza de date
     */
    @RequestMapping(value = "allTrainers")
    public String allTrainers(Model model) {
        List<User> trainers = userService.findAllTrainers();

        model.addAttribute("trainers", trainers);
        model.addAttribute("title","All trainers");
        return "client/allTrainers";
    }


    /**
     *
     * @return  toate clasele la care este abonat
     */
    @RequestMapping(value = "userClasses")
    public String userClasses(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            List<TimetableClass> tc = user.getTimetableClasses();

            SortedSet<TimetableClass> set = new TreeSet<TimetableClass>(new Comparator<TimetableClass>(){

                public int compare(TimetableClass o1, TimetableClass o2) {
                    // return 0 if objects are equal in terms of your properties
                    if (o1.getfClass().getName().equals(o2.getfClass().getName()) && o1.getDate().isEqual(o2.getDate()))
                        return 0;
                    else
                        return 1;
                }
            });

            set.addAll(tc);
            List<TimetableClass> listWithoutDuplicates = new ArrayList<>(set);

            model.addAttribute("tcs", listWithoutDuplicates);
            model.addAttribute("title", "All your subscriptions");
        }
        return "client/userClasses";
    }


    @RequestMapping(value = "addClass", method = RequestMethod.GET)
    public String addClass(Model model, Authentication authentication) {

        Iterable<TimetableClass> classes = timetableClassDao.findAll();

        model.addAttribute("title", "Subscribe");
        model.addAttribute("classes", classes);

        return "/client/addClass";
    }


    /**
     *
     * Adauga o clasa de fitness de pe orar unui client, clasa cu id-ul classId
     */
    @RequestMapping(value = "addClass", method = RequestMethod.POST)
    public String processaAddClass(@RequestParam int classId, Model model, Authentication authentication){
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            TimetableClass timetableClass = timetableClassDao.findById(classId).orElse(new TimetableClass());

            userService.addUserTC(user,timetableClass);


            Iterable<TimetableClass> classes = timetableClassDao.findAll();
            model.addAttribute("message", "SUCCESS!");
            model.addAttribute("classes", classes);
        }


        return "/client/addClass";
    }

    @RequestMapping(value = "removeClass", method = RequestMethod.GET)
    public String removeClass(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            Iterable<TimetableClass> classes = user.getTimetableClasses();

            model.addAttribute("title", "Unsubscribe");
            model.addAttribute("classes", classes);
        }
        return "/client/removeClass";
    }

    /**
     *
     * Dezaboneaza un client de la o clasa cu id-ul classId
     */
    @RequestMapping(value = "removeClass", method = RequestMethod.POST)
    public String processaRemoveClass(@RequestParam int classId, Model model, Authentication authentication){
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            TimetableClass timetableClass = timetableClassDao.findById(classId).orElse(new TimetableClass());

            user.getTimetableClasses().remove(timetableClass);
            timetableClass.decrement();
            timetableClassDao.save(timetableClass);
            userDao.save(user);

            Iterable<TimetableClass> classes = user.getTimetableClasses();
            model.addAttribute("message", "SUCCESS!");
            model.addAttribute("classes", classes);
            //classes.forEach(System.out::println);
        }
        return "/client/removeClass";
    }




}
