package com.utcn.ps2019.sampleproject.controllers;

import com.utcn.ps2019.sampleproject.data.RoleDao;
import com.utcn.ps2019.sampleproject.data.StyleDao;
import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.service.FClassService;
import com.utcn.ps2019.sampleproject.service.RoleService;
import com.utcn.ps2019.sampleproject.service.StyleService;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class MainController {
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private StyleDao styleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @Autowired
    private FClassService fClassService;

    @Autowired
    private StyleService styleService;

    @Autowired
    private RoleService roleService;


    @RequestMapping(value = "login",method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("title","LadyFIT");
        return "login";
    }

    @RequestMapping(value = "/accessDenied")
    public String accessDenied() {
        return "/accessDenied";
    }

    @RequestMapping(value = "logout",method = RequestMethod.GET)
    public String logout() {
        return "redirect:/login";
    }


    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String displayRegisterForm(Model model) {
        model.addAttribute("title","Register");
        model.addAttribute(new User());
        List<Role> my_roles = roleService.findAllButAdmin();
        model.addAttribute("roles", my_roles); //ROLE_ADMIN, ROLE_STUDENT, ROLE_PROF...
        return "/register";
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String processRegisterForm(@ModelAttribute @Valid User user, Errors errors, @RequestParam int roleId, Model model){
        if (errors.hasErrors()){
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            return "/register";
        }
        User userDB = userDao.findByUsername(user.getUsername());
        User userDBB = userDao.findByEmail(user.getEmail());
        if (userDB==null && userDBB==null) {
            userService.addUser(user,roleId);
            model.addAttribute("message", "SUCCESSFUL REGISTRATION!");
        }
        else if (userDB!=null)
        {
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            model.addAttribute("message", "THIS USERNAME IS ALREADY TAKEN");
        }
        else
        {
            model.addAttribute("title","Register");
            List<Role> my_roles = roleService.findAllButAdmin();
            model.addAttribute("roles", my_roles);
            model.addAttribute("message", "THIS EMAIL IS ALREADY TAKEN");
        }
        return "/register";
    }


    @GetMapping(path = "/allTrainers", produces = "application/json")
    public List<User> allTrainers() {
        List<User> trainers = userService.findAllTrainers();

        return trainers;
    }

    @GetMapping(path = "/allRoles",produces = "application/json")
    public List<Role> allRoles() {
        Iterable<Role> myRoles = roleDao.findAll();
        List<Role> roles = new ArrayList<>();
        myRoles.iterator().forEachRemaining(roles::add);

        return roles;
    }

    @GetMapping(path="/getClassesByStyle/{styleName}", produces = "application/json")
    public List<FClass> getClassesByStyle(@PathVariable String styleName) {
        List<FClass> classes = fClassService.getClassesByStyle(styleName);

        return classes;
    }


}
