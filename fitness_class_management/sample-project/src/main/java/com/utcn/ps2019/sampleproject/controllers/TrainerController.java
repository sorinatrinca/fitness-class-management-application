package com.utcn.ps2019.sampleproject.controllers;


import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.TimetableClass;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.service.FClassService;
import com.utcn.ps2019.sampleproject.service.TimetableClassService;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/trainer")
@Controller
@EnableAutoConfiguration
public class TrainerController {
    @Autowired
    private FClassService fClassService;

    @Autowired
    private UserService userService;

    @Autowired
    private FClassDao fClassDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TimetableClassService timetableClassService;

    @Autowired
    private TimetableClassDao timetableClassDao;

    @RequestMapping(value = "homepage")
    public String homepage() {
        return "trainer/homepage";
    }


    /**
     *
     * Returneaza toate clasele de fitness din baza de date
     */
    @RequestMapping(value = "allClasses")
    public String allClasses(Model model) {
        Iterable<FClass> classes = fClassDao.findAll();

        model.addAttribute("classes", classes);
        model.addAttribute("title","All classes");
        return "trainer/allClasses";
    }

    /**
     *
     * Returneaza toate clasele de fitness de pe orar
     */
    @RequestMapping(value = "allTimetableClasses")
    public String allTimetableClasses(Model model) {
        Iterable<TimetableClass> tclasses = timetableClassDao.findAll();

        model.addAttribute("tclasses", tclasses);
        model.addAttribute("title","All timetable classes");
        return "trainer/allTimetableClasses";
    }

    /**
     *
     * Returneaza clasele de fitness la care trainerul curent logat preda
     */
    @RequestMapping(value = "trainerTC")
    public String trainerTC(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            List<TimetableClass> tcs = user.getTimetableClasses();

            model.addAttribute("tclasses", tcs);
            model.addAttribute("title","All trainer TC");
            return "trainer/trainerTC";
        }

        return "trainer/trainerTC";
    }


    /**
     *
     * Returneaza clientii abonati la clasele de fitness la care preda trainerul curent logat
     */
    @RequestMapping(value = "trainerClients")
    public String trainerClients(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            List<User> users = userService.trainerClients(user);

            model.addAttribute("users", users);
            model.addAttribute("title","All trainer's users");
            return "trainer/trainerClients";
        }

        return "trainer/trainerClients";
    }

    @RequestMapping(value = "removeTC", method = RequestMethod.GET)
    public String removeTC(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            List<TimetableClass> tcs = user.getTimetableClasses();

            model.addAttribute("tcs", tcs);
            model.addAttribute("title","Remove TC");
            return "trainer/removeTC";
        }

        return "trainer/removeTC";
    }

    /**
     *
     * Sterge o clasa de pe orar
     */
    @RequestMapping(value = "removeTC", method = RequestMethod.POST)
    public String processRemoveTC(@RequestParam int id, Model model) {
        TimetableClass tc = timetableClassDao.findById(id).orElse(new TimetableClass());
        timetableClassDao.delete(tc);
        return "redirect:/trainer/removeTC";
    }


    @RequestMapping(value = "addTC", method = RequestMethod.GET)
    public String addTC(Model model) {
        model.addAttribute("title","Add TC");
        model.addAttribute("timetableClass",new TimetableClass());
        Iterable<FClass> classes = fClassDao.findAll();
        model.addAttribute("classes", classes);
        return "/trainer/addTC";
    }

    /**
     *
     * Adauga o clasa pe orar, care repzezinta clasa de fitness cu id-ul classId
     */
    @RequestMapping(value = "addTC", method = RequestMethod.POST)
    public String processaddTC(@ModelAttribute @Valid TimetableClass timetableClass, Errors errors, @RequestParam int classId, Model model, Authentication authentication){
        if (!(authentication instanceof AnonymousAuthenticationToken)) {

            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);

            timetableClassService.add(timetableClass, classId, user);
            model.addAttribute("message", "SUCCESSFUL REGISTRATION!");
        }
        return "/trainer/addTC";
    }

    @RequestMapping(value = "addClient", method = RequestMethod.GET)
    public String addClient(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            List<TimetableClass> classes = user.getTimetableClasses();

            model.addAttribute("title", "Add client");
            model.addAttribute("classes", classes);
            //classes.forEach(System.out::println);

            Iterable<User> clients = userService.findAllClients();
            model.addAttribute("clients", clients);
        }

        return "/trainer/addClient";
    }


    /**
     *
     * Adauga un client la o clasa de fitness de pe orar
     */
    @RequestMapping(value = "addClient", method = RequestMethod.POST)
    public String processaAddClient(@RequestParam int classId, @RequestParam int clientId, Model model){
        Iterable<TimetableClass> classes = timetableClassDao.findAll();

        User u = userDao.findById(clientId).orElse(new User());
        TimetableClass t = timetableClassDao.findById(classId).orElse(new TimetableClass());
        t.addUser(u);
        u.addTimetableClass(t);
        //if (!u.getTimetableClasses().contains(t)) {
            userDao.save(u);
            timetableClassDao.save(t);
        //}

        model.addAttribute("message", "SUCCESS!");
        model.addAttribute("classes", classes);
        //classes.forEach(System.out::println);

        Iterable<User> clients = userService.findAllClients();
        model.addAttribute("clients", clients);

        return "/trainer/addClient";
    }


    @RequestMapping(value = "removeClient", method = RequestMethod.GET)
    public String removeClient(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            List<User> clients = userService.trainerClients(user);

            model.addAttribute("clients", clients);
            model.addAttribute("title","Remove client");
        }
        return "/trainer/removeClient";
    }

    /**
     *
     * Sterge un client din cadrul clasei la care trainerul curent logat preda
     */
    @RequestMapping(value = "removeClient", method = RequestMethod.POST)
    public String processaRemoveClient(@RequestParam int clientId, Model model, Authentication authentication){
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);


            User client = userDao.findById(clientId).orElse(new User());
            userService.removeClientFromTrainerClass(user, client);
        }
        return "redirect:/trainer/trainerClients";
    }


    @RequestMapping(value = "classClients", method = RequestMethod.GET)
    public String classClients(Model model, Authentication authentication) {
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String auth = authentication.getName();//returneaza USERNAME
            User user = userDao.findByUsername(auth);
            Iterable<TimetableClass> tcs = user.getTimetableClasses();

            model.addAttribute("tcs", tcs);
            model.addAttribute("title", "Class clients");
        }
        return "trainer/classClients";
    }

    /**
     *
     * Returneaza toti clientii abonati la clasa cu id-ul classId
     */
    @RequestMapping(value = "classClients", method = RequestMethod.POST)
    public String classClientsPost(@RequestParam int classId, Model model) {
        TimetableClass tc = timetableClassDao.findById(classId).orElse(new TimetableClass());

        List<User> users = tc.getUsers();
        List<User> toRemove = new ArrayList<>();
        if (users!=null) {
            for (User u : users) {
                if ("ROLE_TRAINER".equals(u.getRole().getName()))
                    toRemove.add(u);
            }
            users.removeAll(toRemove);
        }

        model.addAttribute("users", users);
        Iterable<TimetableClass> tcs = timetableClassDao.findAll();

        model.addAttribute("tcs", tcs);
        return "trainer/classClients";
    }

}
