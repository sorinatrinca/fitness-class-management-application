package com.utcn.ps2019.sampleproject.controllers.rest_controllers;

import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.service.FClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/class")
@EnableAutoConfiguration
public class FClassController {
    @Autowired
    private FClassService fClassService;

    @Autowired
    private FClassDao fClassDao;

    /**
     *
     * @return toate clasele din baza de date
     */
    /*@GetMapping(path = "/allClasses", produces = "application/json")
    public Iterable<FClass> allClasses() {
        return fClassDao.findAll();
    }*/

    @RequestMapping(value = "allClasses")
    public String allClasses(Model model) {
        Iterable<FClass> classes = fClassDao.findAll();

        model.addAttribute("classes", classes);
        model.addAttribute("title","All classes");
        return "class/allClasses";
    }


    /**
     *
     * @param fClass clasa de fitness pe care vrem sa o adaugam in baza de date
     * @return clasa adaugata in baza de date
     */
    @PostMapping(path = "/createClass", produces = "application/json")
    public FClass createClass(@RequestBody FClass fClass)
    {
        fClassService.createClass(fClass);
        return fClass;
    }

    //idempotent - update daca exista resursa deja

    /**
     *
     * @param id id-ul clasei de fitness din baza de date pe care dorim sa o actualizam
     * @param newClass informatiile necesare actualizarii, stocate intr-un obiect al clasei necesare
     * @return clasa de fitness actualizata
     */
    @PutMapping(path = "/updateClass/{id}", produces = "application/json")
    public FClass updateClass(@PathVariable int id, @RequestBody FClass newClass)
    {
        fClassService.update(id,newClass);
        return newClass;
    }

    /**
     *
     * @param fClass clasa de fitness pe care dorim sa o stergem
     * @return clasa de fitness pe care am sters-o din baza de date
     */
    @DeleteMapping(path = "/deleteClass", produces = "application/json")
    public FClass deleteClass(@RequestBody FClass fClass)
    {
        fClassService.deleteClass(fClass);
        return fClass;
    }

    /**
     * Stergerea unei clase de fitness din baza de date
     * @param id id-ul clasei de fitness pe care dorim sa o stergem
     */
    @DeleteMapping(path = "/deleteClassById/{id}", produces = "application/json")
    public void deleteClassById(@PathVariable int id)
    {
        fClassDao.deleteById(id);
    }


    /**
     *
     * @param styleName numele stilului pe care il au clasele de fitness pe care le returnam
     * @return o lista cu clasele de fitness care au stilul dat ca parametru
     */
    @GetMapping(path="/getClassesByStyle/{styleName}", produces = "application/json")
    public List<FClass> getClassesByStyle(@PathVariable String styleName) {
        //System.out.println(styleName);
        List<FClass> classes = fClassService.getClassesByStyle(styleName);

        return classes;
    }

}

