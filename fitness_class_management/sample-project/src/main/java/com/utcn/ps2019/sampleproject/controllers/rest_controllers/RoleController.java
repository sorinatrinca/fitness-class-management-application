package com.utcn.ps2019.sampleproject.controllers.rest_controllers;


import com.utcn.ps2019.sampleproject.data.RoleDao;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleDao roleDao;

    /**
     *
     * @return toate rolurile stocate in baza de date
     */
    @GetMapping(path = "/allRoles", produces = "application/json")
    public Iterable<Role> allRoles() {
        return roleDao.findAll();
    }

    /**
     * Adaugarea unui rol in baza de date
     * @param role obiectul de tip rol pe care vrem sa-l introducem in baza de date
     * @return obiectul adaugat
     */
    @PostMapping(path = "/createRole", produces = "application/json")
    public Role createRole(@RequestBody Role role)
    {
        roleService.createRole(role);
        //System.out.println(style.getName()+" "+style.getId());
        return role;
    }

    //idempotent - update daca exista resursa deja
    /**
     *
     * @param id id-ul rolului din baza de date pe care dorim sa-l actualizam
     * @param newRole informatiile necesare actualizarii, stocate intr-un obiect al clasei necesare
     * @return obiectul actualizat
     */
    @PutMapping(path = "/updateRole/{id}", produces = "application/json")
    public Role updateRole(@PathVariable int id, @RequestBody Role newRole)
    {
        roleService.update(id,newRole);
        return newRole;
    }

    /**
     *
     * @param role rolul pe care dorim sa-l stergem
     * @return rolul pe care l-am sters din baza de date
     */
    @DeleteMapping(path = "/deleteRole", produces = "application/json")
    public Role deleteRole(@RequestBody Role role)
    {
        roleService.deleteRole(role);
        return role;
    }

    /**
     * Stergerea unui rol din baza de date
     * @param id id-ul rolului pe care dorim sa-l stergem
     */
    @DeleteMapping(path = "/deleteRoleById/{id}", produces = "application/json")
    public void deleteRoleById(@PathVariable int id)
    {
        roleDao.deleteById(id);
    }
}
