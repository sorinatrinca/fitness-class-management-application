package com.utcn.ps2019.sampleproject.controllers.rest_controllers;

import com.utcn.ps2019.sampleproject.data.StyleDao;
import com.utcn.ps2019.sampleproject.models.Style;
import com.utcn.ps2019.sampleproject.service.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;


@RestController
@EnableAutoConfiguration
@RequestMapping("/style")
public class StyleController {
    @Autowired
    private StyleService styleService;

    @Autowired
    private StyleDao styleDao;

    /**
     *
     * @return o lista cu toate stilurile din baza de date
     */
    @GetMapping(path = "/allStyles", produces = "application/json")
    public Iterable<Style> allStyles() {
        return styleDao.findAll();
    }

    /**
     * Adauharea unui stil de fitness in baza de date
     * @param style obiectul de tip Style pe care vrem sa-l introducem
     * @return obiectul adaugat
     */
    @PostMapping(path = "/createStyle", produces = "application/json")
    public Style createStyle(@RequestBody Style style)
    {
        styleService.createStyle(style);
        //System.out.println(style.getName()+" "+style.getId());
        return style;
    }

    //idempotent - update daca exista resursa deja
    /**
     *
     * @param id id-ul stilului din baza de date pe care dorim sa-l actualizam
     * @param newStyle informatiile necesare actualizarii, stocate intr-un obiect al clasei necesare
     * @return obiectul actualizat
     */
    @PutMapping(path = "/updateStyle/{id}", produces = "application/json")
    public Style updateStyle(@PathVariable int id, @RequestBody Style newStyle)
    {
        styleService.update(id,newStyle);
        return newStyle;
    }

    /**
     *
     * @param style stilul pe care dorim sa-l stergem
     * @return stilul pe care l-am sters din baza de date
     */
    @DeleteMapping(path = "/deleteStyle", produces = "application/json")
    public Style deleteStyle(@RequestBody Style style)
    {
        styleService.deleteStyle(style);
        return style;
    }

    /**
     * Stergerea unui stil de fitness din baza de date
     * @param id id-ul stilului pe care dorim sa-l stergem
     */
    @DeleteMapping(path = "/deleteStyleById/{id}", produces = "application/json")
    public void deleteStyleById(@PathVariable int id)
    {
        styleDao.deleteById(id);
    }
}
