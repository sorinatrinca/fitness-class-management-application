package com.utcn.ps2019.sampleproject.controllers.rest_controllers;


import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.models.TimetableClass;
import com.utcn.ps2019.sampleproject.service.TimetableClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@EnableAutoConfiguration
@RequestMapping("/timetable")
public class TimetableClassController {
    @Autowired
    private TimetableClassService timetableClassService;

    @Autowired
    private TimetableClassDao timetableClassDao;

    /**
     *
     * @return o lista cu toate clasele de fitness de pe orar, din baza de date
     */
    /*@GetMapping(path = "/allTimetableClasses", produces = "application/json")
    public Iterable<TimetableClass> allTimetableClasses() {
        return timetableClassDao.findAll();
    }*/

    @RequestMapping(value = "allTimetableClasses")
    public String allTimetableClasses(Model model) {
        Iterable<TimetableClass> tclasses = timetableClassDao.findAll();
        List<TimetableClass> tc = new ArrayList<>();
        tclasses.forEach(tc::add);
        System.out.println(tc.get(0).getDate());
        model.addAttribute("tclasses", tclasses);
        model.addAttribute("title","All timetable classes");
        return "timetable/allTimetableClasses";
    }

    /**
     * Adauharea unei clase de fitness de pe orar
     * @param timetableClass obiectul de tip TimetableClass pe care vrem sa-l introducem
     * @return obiectul adaugat
     */
    @PostMapping(path = "/createTimetableClass", produces = "application/json")
    public TimetableClass createTimetableClass(@RequestBody TimetableClass timetableClass)
    {
        timetableClassDao.save(timetableClass);
        return timetableClass;
    }

    //idempotent - update daca exista resursa deja
    /**
     *
     * @param id id-ul rolului din baza de date pe care dorim sa-l actualizam
     * @param timetableClass informatiile necesare actualizarii, stocate intr-un obiect al clasei necesare
     * @return obiectul actualizat
     */
    @PutMapping(path = "/updateTimetableClass/{id}", produces = "application/json")
    public TimetableClass updateTimetableClass(@PathVariable int id, @RequestBody TimetableClass timetableClass)
    {
        timetableClassService.update(id,timetableClass);
        return timetableClass;
    }

    /**
     *
     * @param timetableClass clasa de fitness din orar pe care dorim sa o stergem
     * @return clasa de fitness de pe orar pe care am sters-o din baza de date
     */
    @DeleteMapping(path = "/deleteTimetableClass", produces = "application/json")
    public TimetableClass deleteTimetableClass(@RequestBody TimetableClass timetableClass)
    {
        timetableClassService.deleteTimetableClass(timetableClass);
        return timetableClass;
    }

    /**
     * Stergerea unei clase de fitness de pe orar din baza de date
     * @param id id-ul clasei de pe orar pe care dorim sa o stergem
     */
    @DeleteMapping(path = "/deleteTimetableClassById/{id}", produces = "application/json")
    public void deleteTimetableClassById(@PathVariable int id)
    {
        timetableClassDao.deleteById(id);
    }
}
