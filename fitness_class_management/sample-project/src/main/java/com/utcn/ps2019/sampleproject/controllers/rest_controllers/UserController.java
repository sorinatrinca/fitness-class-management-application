package com.utcn.ps2019.sampleproject.controllers.rest_controllers;

import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.User;
import com.utcn.ps2019.sampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
@Controller
@EnableAutoConfiguration
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    /**
     * Adaugarea unui User in baza de date
     * @param user obiectul de tip User pe care vrem sa-l adaugam in baza de date
     * @return userul adaugat in baza de date
     */
    @PostMapping(path = "/createUser", produces = "application/json")
    public User createUser(@RequestBody User user)
    {
        userDao.save(user);
        return user;
    }

    /**
     *
     * @return lista de useri din baza de date
     */
    /*
    @GetMapping(path = "/allUsers", produces = "application/json")
    public Iterable<User> allUsers()
    {
        return userDao.findAll();
    }*/

    @RequestMapping(value = "allUsers")
    public String allUsers(Model model) {
        Iterable<User> users = userDao.findAll();

        model.addAttribute("users", users);
        model.addAttribute("title","All users");
        return "user/allUsers";
    }


    /**
     *
     * @return lista de traineri din baza de date
     */
    /*@GetMapping(path = "/allTrainers", produces = "application/json")
    public List<User> allTrainers() {
        List<User> trainers = userService.findAllTrainers();

        return trainers;
    }*/

    @RequestMapping(value = "allTrainers")
    public String allTrainers(Model model) {
        List<User> trainers = userService.findAllTrainers();

        model.addAttribute("trainers", trainers);
        model.addAttribute("title","All trainers");
        return "user/allTrainers.html";
    }

    /**
     *
     * @return lista de clientu din baza de date
     */
    /*@GetMapping(path = "/allClients", produces = "application/json")
    public List<User> allClients() {
        List<User> clients = userService.findAllClients();

        return clients;
    }*/

    @RequestMapping(value = "allClients")
    public String allClients(Model model) {
        List<User> clients = userService.findAllClients();

        model.addAttribute("clients", clients);
        model.addAttribute("title","All clients");
        return "user/allClients.html";
    }



    @GetMapping(path = "/raport/{id}", produces = "application/json")
    public List<Object> raport(@PathVariable int id) {
        return userService.raport(id);
    }

    /**
     *
     * @param id id-ul userului din baza de date pe care dorim sa-l actualizam
     * @param newUser informatiile necesare actualizarii, stocate intr-un obiect al clasei necesare
     * @return obiectul actualizat
     */
    @PutMapping(path = "/updateUser/{id}", produces = "application/json")
    public User updateUser(@PathVariable int id, @RequestBody User newUser)
    {
        userService.update(id,newUser);
        return newUser;
    }


    /**
     * Stergerea unui User in baza de date
     * @param id este id-ul userului pe care vrem sa-l stergem
     */
    @DeleteMapping(path = "/deleteUser/{id}", produces = "application/json")
    public void deleteUserById(@PathVariable int id)
    {
        userDao.deleteById(id);
    }


}
