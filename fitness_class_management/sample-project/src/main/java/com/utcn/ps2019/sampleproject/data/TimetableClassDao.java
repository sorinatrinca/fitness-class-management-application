package com.utcn.ps2019.sampleproject.data;

import com.utcn.ps2019.sampleproject.models.TimetableClass;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
@EnableJpaRepositories
public interface TimetableClassDao extends CrudRepository<TimetableClass, Integer> {
    //TimetableClass findByD
}
