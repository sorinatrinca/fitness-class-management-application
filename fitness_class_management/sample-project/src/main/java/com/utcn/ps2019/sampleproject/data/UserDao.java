package com.utcn.ps2019.sampleproject.data;

import com.utcn.ps2019.sampleproject.models.User;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
@EnableJpaRepositories
public interface UserDao extends CrudRepository<User, Integer> {
        User findByUsername(String username);
        User findByEmail(String email);
}
