package com.utcn.ps2019.sampleproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

public abstract class AbstractUser<T> extends User {

    public AbstractUser() {
        super();
    }

    public AbstractUser(int id, @NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(id, firstName, lastName, username, password, email);
    }

    public AbstractUser(@NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(firstName, lastName, username, password, email);
    }


    public abstract List<T> raport();
}
