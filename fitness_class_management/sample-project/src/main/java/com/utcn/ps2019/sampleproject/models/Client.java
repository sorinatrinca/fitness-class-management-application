package com.utcn.ps2019.sampleproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

public class Client extends AbstractUser implements Observer{

    public Client(@NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(firstName, lastName, username, password, email);
    }

    public Client(int id, @NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(id, firstName, lastName, username, password, email);
    }

    public Client(){}

    @Override
    public void update() {
        String s = "The timetable has changed! Please take a look :)" + LocalDate.now().toString();
        //super.addNotification(s);
    }

    public List<TimetableClass> raport()
    {
        return super.getTimetableClasses();
    }

    @Override
    public void setFirstName(String firstName) {
        super.setFirstName(firstName);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String email) {
        super.setEmail(email);
    }

    @Override
    public Role getRole() {
        return super.getRole();
    }

    @Override
    public void setRole(Role role) {
        super.setRole(role);
    }

    @Override
    public List<TimetableClass> getTimetableClasses() {
        return super.getTimetableClasses();
    }

    @Override
    public void setTimetableClasses(List<TimetableClass> classes) {
        super.setTimetableClasses(classes);
    }


}
