package com.utcn.ps2019.sampleproject.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class FClass {
    @Id
    @GeneratedValue
    private int id;


    //@NotNull
    @Size(min=2, max=20)
    private String name;

    @NotNull
    private StrengthType strength;

    @ManyToOne
    @JoinColumn
    private Style style;

    @OneToMany(mappedBy = "fClass", cascade = CascadeType.ALL)
    private List<TimetableClass> timetableClasses;

    public FClass(int id, @NotNull @Size(min = 2, max = 20) String name, @NotNull StrengthType strength) {
        this.id = id;
        this.name = name;
        this.strength = strength;
    }

    public FClass(@NotNull @Size(min = 2, max = 20) String name, @NotNull StrengthType strength) {
        this.name = name;
        this.strength = strength;
    }

    public FClass(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StrengthType getStrength() {
        return strength;
    }

    public void setStrength(StrengthType strength) {
        this.strength = strength;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public List<TimetableClass> getTimetableClasses() {
        return timetableClasses;
    }

    public void setTimetableClasses(List<TimetableClass> timetableClasses) {
        this.timetableClasses = timetableClasses;
    }
}
