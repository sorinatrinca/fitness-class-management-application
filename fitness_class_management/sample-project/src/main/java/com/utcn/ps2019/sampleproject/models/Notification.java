package com.utcn.ps2019.sampleproject.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Notification {
    @Id
    @GeneratedValue
    private int id;

    private String msg;

    @ManyToMany(mappedBy = "notifications")//, cascade = CascadeType.ALL
    private List<User> users;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Notification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
