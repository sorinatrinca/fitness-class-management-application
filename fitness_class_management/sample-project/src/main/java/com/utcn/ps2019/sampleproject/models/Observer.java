package com.utcn.ps2019.sampleproject.models;

public interface Observer {
    void update();
}
