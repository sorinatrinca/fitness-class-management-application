package com.utcn.ps2019.sampleproject.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Role {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Column(unique = true)
    @Size(min=4, max=20)
    private String name;

    @OneToMany(mappedBy = "role")
    private List<User> users;

    public Role(@NotNull @Size(min = 4, max = 20) String name) {
        this.name = name;
    }

    public Role(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
