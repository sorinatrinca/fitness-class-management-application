package com.utcn.ps2019.sampleproject.models;

public class RoleTypeException extends Exception {
    public RoleTypeException(String message)
    {
        super(message);
    }
}
