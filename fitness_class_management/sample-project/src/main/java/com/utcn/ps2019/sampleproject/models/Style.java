package com.utcn.ps2019.sampleproject.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Style {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=2, max=20)
    private String name;

    @OneToMany(mappedBy = "style",cascade = CascadeType.ALL)//cascade = CascadeType.ALL
    private List<FClass> classes;

    public Style(int id, @NotNull @Size(min = 2, max = 20) String name) {
        this.id = id;
        this.name = name;
    }

    public Style(@NotNull @Size(min = 2, max = 20) String name) {
        this.name = name;
    }

    public Style(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
