package com.utcn.ps2019.sampleproject.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TimetableClass  {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn
    private FClass fClass;

    @ManyToMany(mappedBy = "timetableClasses")//, cascade = CascadeType.ALL
    private List<User> users;


    private int maxClients;
    private int numberOfClients;

    //ca sa nu fie stocat in BD
    //@Transient
    //private List<Observer> observers;

    public TimetableClass(int id, @NotNull LocalDateTime date, int maxClients) {
        this.id = id;
        this.date = date;
        //observers = new ArrayList<>();
        this.maxClients = maxClients;
        this.numberOfClients=0;
    }

    public TimetableClass(@NotNull LocalDateTime date, FClass fClass, int maxClients) {
        this.fClass=fClass;
        this.date = date;
        //observers = new ArrayList<>();
        this.numberOfClients=0;
        this.maxClients = maxClients;
    }

    public TimetableClass(int id, @NotNull LocalDateTime date, FClass fClass, int maxClients) {
        this.fClass=fClass;
        this.date = date;
        this.id=id;
        //observers = new ArrayList<>();
        this.numberOfClients=0;
        this.maxClients = maxClients;
    }

    public TimetableClass(){}

    //public void addObserver(Observer observer) {
        //this.observers.add(observer);
    //}

   // public void removeObserver(Observer observer) {
       // this.observers.remove(observer);
    //}

    public void notifyAllObservers(){
        System.out.println("notify");
        if (users !=null) {
            System.out.println("notify yesss");
            for (Observer observer : users) {
                observer.update();
            }
        }
    }
/*
    public void notifyTrainers(){
        for (Observer observer: observers)
        {
            if (observer instanceof Trainer)
                observer.update();
        }
    }

    public void notifyClients(){
        for (Observer observer: observers)
        {
            if (observer instanceof Client)
                observer.update();
        }
    }
*/
    public void addUser(User user)
    {
        if (users == null)
            users = new ArrayList<>();

        if (numberOfClients < maxClients)
        {
            users.add(user);
            numberOfClients++;
            //notifyTrainers();
        }
        //if (observers==null)
          //  observers = new ArrayList<>();
        //observers.add(user);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
        System.out.println("set date");
        //notifyAllObservers();
    }

    public FClass getfClass() {
        return fClass;
    }

    public void setfClass(FClass fClass) {
        this.fClass = fClass;
        //notifyClients();
    }

    public void decrement(){ this.numberOfClients = this.numberOfClients - 1 ;}
    public void incremen(){ this.numberOfClients = this.numberOfClients + 1 ;}

    public int getMaxClients() {
        return maxClients;
    }

    public void setMaxClients(int maxClients) {
        this.maxClients = maxClients;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }
}
