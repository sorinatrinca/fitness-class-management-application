package com.utcn.ps2019.sampleproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class Trainer extends AbstractUser implements Observer{

    public Trainer(int id, @NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(id, firstName, lastName, username, password, email);
    }

    public Trainer(@NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        super(firstName, lastName, username, password, email);
    }

    public Trainer(){}

    @Override
    public void update() {
        String s = "A new client has subscribed to a class!";
        //super.addNotification(s);
    }

    public List<Client> raport()
    {
        List<TimetableClass> classes = getTimetableClasses();
        List<Client> clients = new ArrayList<>();
        for (TimetableClass tc : classes)
        {
            List<User> fcClients = tc.getUsers();
            for (User u : fcClients)
                if (u instanceof Client)
                    if (!clients.contains(u))
                        clients.add((Client)u);

        }
        return clients;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getFirstName() {
        return super.getFirstName();
    }

    @Override
    public void setFirstName(String firstName) {
        super.setFirstName(firstName);
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public void setLastName(String lastName) {
        super.setLastName(lastName);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String email) {
        super.setEmail(email);
    }

    @Override
    public Role getRole() {
        return super.getRole();
    }

    @Override
    public void setRole(Role role) {
        super.setRole(role);
    }

    @Override
    public List<TimetableClass> getTimetableClasses() {
        return super.getTimetableClasses();
    }

    @Override
    public void setTimetableClasses(List<TimetableClass> classes) {
        super.setTimetableClasses(classes);
    }

}
