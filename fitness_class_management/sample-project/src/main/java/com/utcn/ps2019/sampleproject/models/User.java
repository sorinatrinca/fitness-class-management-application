package com.utcn.ps2019.sampleproject.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
public class User implements Observer{

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Size(min=2, max=50)
    private String firstName;

    @NotNull
    @Size(min=2, max=50)
    private String lastName;

    @NotNull
    @Column(unique = true)
    @Size(min=4, max=50)
    private String username;

    @NotNull
    @Size(min=4, max=200)
    private String password;

    @NotNull
    @Column(unique = true)
    @Size(min=6, max=50)
    @Pattern(regexp ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;

    //@ElementCollection
    //@CollectionTable(name = "user_notification", joinColumns = @JoinColumn(name = "id"))
    //private List<String> notification = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "class_users",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "class_id", referencedColumnName = "id")
    )
    private List<TimetableClass> timetableClasses;

    @ManyToMany
    @JoinTable(
            name = "users_notifications",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "notification_id", referencedColumnName = "id")
    )
    private List<Notification> notifications;

    @ManyToOne
    @JoinColumn
    private Role role;

    public User(){}

    public User(int id, @NotNull @Size(min = 2, max = 50) String firstName, @NotNull @Size(min = 2, max = 50) String lastName, @NotNull @Size(min = 4, max = 50) String username, @NotNull @Size(min = 4, max = 50) String password, @NotNull @Size(min = 6, max = 50) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public User( @NotNull @Size(min = 2, max = 50) String firstName, @NotNull @Size(min = 2, max = 50) String lastName, @NotNull @Size(min = 4, max = 50) String username, @NotNull @Size(min = 4, max = 50) String password, @NotNull @Size(min = 6, max = 50) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    //pot sa pun observer si invers. In momentul in care adaug o clasa unui user, sa se updateze numarul de oameni de la acea clasa
    public void addTimetableClass(TimetableClass tc)
    {
        timetableClasses.add(tc);
    }

    public void addNotification(Notification n)
    {
        notifications.add(n);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof User)) {
            return false;
        }

        User c = (User) o;
        return ((username.equals(c.getUsername())) && (email.equals(c.getEmail())) );
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<TimetableClass> getTimetableClasses() {
        return timetableClasses;
    }

    public void setTimetableClasses(List<TimetableClass> classes) {
        this.timetableClasses = classes;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotification(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public void update() {
        String s = "The timetable has changed! Please take a look :)    [" + LocalDate.now().toString()+"]";
        Notification n = new Notification();
        n.setMsg(s);
        //System.out.println(s);
        addNotification(n);
    }
}
