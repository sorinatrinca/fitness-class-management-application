package com.utcn.ps2019.sampleproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserFactory {
    /**
     *
     * @param roleName numele rolului pe care il va avea obiectul
     * @return un obiect User, in functie de rolul dat de utilizator
     * @throws RoleTypeException daca nici unul din cele 3 roluri nu este primit ca parametru
     */
    public User createUserByRole(String roleName) throws RoleTypeException{
        User user = null;

        if (roleName.equals("ROLE_CLIENT")) {
            user = new Client();
        }
        if (roleName.equals("ROLE_TRAINER")) {
            user = new Trainer();
        }
        if (roleName.equals("ROLE_ADMIN")) {
            user = new User();
        }
        if (user == null)
            throw new RoleTypeException("Exception: Wrong role input");
        return user;
    }

    public User createFullUserByRole(String roleName, int id, @NotNull @Size(min = 2, max = 20) String firstName, @NotNull @Size(min = 2, max = 20) String lastName, @NotNull @Size(min = 4, max = 20) String username, @NotNull @Size(min = 4, max = 20) String password, @NotNull @Size(min = 6, max = 20) @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$") String email) throws RoleTypeException {
        User user = null;

        if (roleName.equals("ROLE_CLIENT")) {
            user = new Client(id, firstName, lastName, username, password, email);
        }
        if (roleName.equals("ROLE_TRAINER")) {
            user = new Trainer(id, firstName, lastName, username, password, email);
        }
        if (roleName.equals("ROLE_ADMIN")) {
            user = new User(id, firstName, lastName, username, password, email);
        }
        if (user == null)
            throw new RoleTypeException("Exception: Wrong role input");
        return user;
    }
}