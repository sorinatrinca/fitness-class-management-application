package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FClassService {
    @Autowired
    private FClassDao fClassDao;


    /**
     * Furnizeaza toate clasele care au un anumit stil
     * @param styleName numele stilului unei clase de fitness
     * @return clasele de fitness din baza de date care au stilul dat ca parametru
     */
    public List<FClass> getClassesByStyle(String styleName)
    {
        Iterable<FClass> myClasses = fClassDao.findAll();
        List<FClass> classes = new ArrayList<>();
        for (FClass f: myClasses)
        {
            if (f.getStyle().getName().equals(styleName))
            {
                classes.add(f);
            }
        }

        return classes;
    }

    /**
     * Stocheaza in baza de date un obiect de tip FClass
     * @param fClass obiectul de tip FClass care trebuie stocat
     */
    public void createClass(FClass fClass)
    {
        fClassDao.save(fClass);
    }


    /**
     * Face update in baza de date la obiect de tip FClass al carui id este dat ca parametru, utilizand continutul unui obiect de acest tip
     * @param id id-ul obiectului din baza de date pe care vrem sa-l modificam
     * @param fClass un obiect care mentine datele cu care vrem sa actualizam obiectul din baza de date
     */
    public void update(int id, FClass fClass)
    {
        FClass s = fClassDao.findById(id).orElse(new FClass());
        s.setName(fClass.getName());
        s.setStrength(fClass.getStrength());
        s.setStyle(fClass.getStyle());
        fClassDao.save(s);
    }

    /**
     * Sterge din baza de date obiectul dat ca parametru
     * @param fClass obiectul pe care vrem sa-l stergem
     */
    public void deleteClass(FClass fClass)
    {
        fClassDao.delete(fClass);
    }
}
