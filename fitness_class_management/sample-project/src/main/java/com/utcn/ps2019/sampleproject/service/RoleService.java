package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.RoleDao;
import com.utcn.ps2019.sampleproject.models.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleDao roleDao;

    /**
     * Stocheaza in baza de date un obiect de tip Role
     * @param role este un obiect de tipul Role pe care vrem sa-l adaugam in baza de date
     */
    public void createRole(Role role)
    {
        roleDao.save(role);
    }

    /**
     * Sterge din baza de date obiectul dat ca parametru
     * @param role obiectul pe care vrem sa-l stergem
     */
    public void deleteRole(Role role)
    {
        roleDao.delete(role);
    }


    /**
     * Face update in baza de date la obiect de tip Role al carui id este dat ca parametru, utilizand continutul unui obiect de acest tip
     * @param id id-ul obiectului din baza de date pe care vrem sa-l modificam
     * @param role obiectul de tip Role care contine informatiile necesare updatarii obiectului role din baza de date cu id-ul "id"
     */
    public void update(int id, Role role) {
        Role r = roleDao.findById(id).orElse(new Role());
        r.setName(role.getName());
        roleDao.save(r);
    }

    /**
     * Se parcurg toate rolurile din baza de date. Daca rolul nu este de admin, atunci se adauga in lista ce va fi furnizata
     * @return toate rolurile, mai putin cel de ADMIN
     */
    public List<Role> findAllButAdmin()
    {
        Iterable<Role> roles = roleDao.findAll();
        List<Role> myRoles = new ArrayList<>();
        for (Role r: roles)
            if (!("ROLE_ADMIN".equals(r.getName())))
                myRoles.add(r);

        return myRoles;
    }
}
