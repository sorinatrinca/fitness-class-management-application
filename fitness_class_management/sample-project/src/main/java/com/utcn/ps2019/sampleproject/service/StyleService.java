package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.StyleDao;
import com.utcn.ps2019.sampleproject.models.Style;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StyleService {
    @Autowired
    private StyleDao styleDao;

    /**
     * Stocheaza in baza de date un obiect de tip FClass
     * @param style obiectul de tip FClass care trebuie stocat
     */
    public void createStyle(Style style)
    {
        styleDao.save(style);
    }

    /**
     * Sterge din baza de date obiectul dat ca parametru
     * @param style obiectul pe care vrem sa-l stergem
     */
    public void deleteStyle(Style style)
    {
        styleDao.delete(style);
    }


    /**
     * Face update in baza de date la obiect de tip Style al carui id este dat ca parametru, utilizand continutul unui obiect de acest tip
     * @param id id-ul obiectului din baza de date pe care vrem sa-l modificam
     * @param style obiectul de tip Style care contine informatiile necesare updatarii obiectului style din baza de date cu id-ul "id"
     */
    public void update(int id, Style style)
    {
        Style s = styleDao.findById(id).orElse(new Style());
        s.setName(style.getName());
        styleDao.save(s);
    }



}
