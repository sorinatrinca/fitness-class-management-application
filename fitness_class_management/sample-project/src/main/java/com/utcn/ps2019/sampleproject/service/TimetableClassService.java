package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.TimetableClass;
import com.utcn.ps2019.sampleproject.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimetableClassService {
    @Autowired
    private TimetableClassDao timetableClassDao;
    @Autowired
    private FClassDao fClassDao;

    @Autowired
    private UserDao userDao;

    /**
     * Furnizeaza o lista cu toate clasele din orar care apartin aceleasi clase de fitness
     * @param className numele clasei de fitness
     * @return clasele de fitness de pe orar care corespund clasei de fitness date ca parametru
     */
    public List<TimetableClass> getTimetableClassesByType(String className)
    {
        Iterable<TimetableClass> myClasses = timetableClassDao.findAll();
        List<TimetableClass> timetableClasses = new ArrayList<>();
        for(TimetableClass tc: myClasses)
        {
            if (tc.getfClass().getName().equals(className))
                timetableClasses.add(tc);
        }
        return timetableClasses;
    }

    /**
     * Stocheaza in baza de date un obiect de tip FClass
     * @param timetableClass obiectul de tip FClass care trebuie stocat
     */
    public void createTimetableClass(TimetableClass timetableClass)
    {
        timetableClassDao.save(timetableClass);
    }

    /**
     * Sterge din baza de date obiectul dat ca parametru
     * @param timetableClass obiectul pe care vrem sa-l stergem
     */
    public void deleteTimetableClass(TimetableClass timetableClass)
    {
        timetableClassDao.delete(timetableClass);
    }


    /**
     * Face update in baza de date la obiect de tip TimetableClass al carui id este dat ca parametru, utilizand continutul unui obiect de acest tip
     * @param id id-ul obiectului din baza de date pe care vrem sa-l modificam
     * @param timetableClass obiectul de tip Style care contine informatiile necesare updatarii obiectului timetableClass din baza de date cu id-ul "id"
     */
    public void update(int id, TimetableClass timetableClass)
    {
        TimetableClass t = timetableClassDao.findById(id).orElse(new TimetableClass());
        t.setDate(timetableClass.getDate());
        t.setfClass(timetableClass.getfClass());
        timetableClassDao.save(t);
    }

    /**
     *
     * @param tc clasa de fitness din orar
     * @param classId id-ul clasei de fitness pe care tc o reprezinta in orar
     * @param user utilizatorul care preda sau care se aboneaza la acea clasa de fitness din orar
     */
    public void add(TimetableClass tc, int classId, User user)
    {
        FClass fclass = fClassDao.findById(classId).orElse(new FClass());
        tc.setfClass(fclass);

        List<User> listUsers = new ArrayList<>();
        listUsers.add(user);
        tc.setUsers(listUsers);

        if ( user.getTimetableClasses()==null){
            List<TimetableClass> listTimetable = new ArrayList<>();
            listTimetable.add(tc);
            user.setTimetableClasses(listTimetable);
        }
        else
            user.getTimetableClasses().add(tc);

        timetableClassDao.save(tc);
        userDao.save(user);
    }
}
