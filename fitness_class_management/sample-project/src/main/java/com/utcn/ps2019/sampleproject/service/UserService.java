package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.RoleDao;
import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    TimetableClassDao timetableClassDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Furnizeaza o lista cu toti utilizatorii din baza de date care sunt traineri
     * @return toti userii din baza de date care au rolul de Trainer
     */
    public List<User> findAllTrainers()
    {
        Iterable<User> users = userDao.findAll();
        List<User> trainers = new ArrayList<>();

        for(User user: users)
        {
            Role role = user.getRole();
            if (role !=null)
                if ("ROLE_TRAINER".equals(user.getRole().getName()))
                {
                    trainers.add(user);
                }
        }
        return trainers;
    }

    /**
     * Furnizeaza o lista cu toti utilizatorii din baza de date care sunt clienti
     * @return toti userii din baza de date care au rolul de Client
     */
    public List<User> findAllClients()
    {
        Iterable<User> users = userDao.findAll();
        List<User> trainers = new ArrayList<>();

        for(User user: users)
        {
            Role role = user.getRole();
            if (role !=null)
                if ("ROLE_CLIENT".equals(user.getRole().getName()))
                {
                    trainers.add(user);
                }
        }
        return trainers;
    }


    /**
     *
     * @param id id-ul utilizatorului caruia vrem sa-i realizam raportul
     * @return o lista cu obiecte specifice raportului ficarui utilizator (de exemplu clientii unui trainer)
     */
    public List<Object> raport(int id){
        User user = userDao.findById(id).orElse(new User());
        Role role = user.getRole();

        List<Object> objects = null;
        if ("ROLE_TRAINER".equals(role.getName()))
        {
            objects = new ArrayList<>(((Trainer)user).raport());
        }
        else if ("ROLE_CLIENT".equals(role.getName()))
        {
            objects = new ArrayList<>(((Client)user).raport());
        }

        return objects;
    }

    /**
     * Face update in baza de date la obiect de tip User al carui id este dat ca parametru, utilizand continutul unui obiect de acest tip
     * @param id id-ul obiectului din baza de date pe care vrem sa-l modificam
     * @param user obiectul de tip Style care contine informatiile necesare updatarii obiectului user din baza de date cu id-ul "id"
     */
    public void update(int id, User user)
    {
        User u = userDao.findById(id).orElse(new User());
        u.setFirstName(user.getFirstName());
        u.setLastName(user.getLastName());
        u.setRole(user.getRole());
        u.setEmail(user.getEmail());
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        userDao.save(u);
    }

    /**
     * Adauga un utilizator in baza de date cu un anumit rol
     * @param user utilizatorul pe care dorim sa-l adaugam
     * @param roleId id-ul rolului pe care il va avea utilizatorul
     */
    public void addUser(User user, int roleId)
    {
        Role role = roleDao.findById(roleId).orElse(new Role());
        user.setRole(role);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    /**
     *
     * @param user trainerul
     * @return lista cu clientii pe care ii are un trainer la clasele acestuia
     */
    public List<User> trainerClients(User user)
    {
        List<TimetableClass> tcs = user.getTimetableClasses();
        List<User> users = new ArrayList<>();

        for (TimetableClass t: tcs)
            users.addAll(t.getUsers());

        List<User> toRemove = new ArrayList<>();
        if (users!=null) {
            for (User u : users) {
                if ("ROLE_TRAINER".equals(u.getRole().getName()))
                    toRemove.add(u);
            }
            users.removeAll(toRemove);
        }

        return users;
    }

    /**
     * Sterge un anumit client din clasele unui trainer
     * @param trainer trainerul de la care vrem sa stergem clientul
     * @param client clientul care va fi sters din clasele trainerului
     */
    public void removeClientFromTrainerClass(User trainer, User client)
    {
        List<TimetableClass> tcTrainer = trainer.getTimetableClasses(); // clasele la care preda trainerul
        List<TimetableClass> tcClient = client.getTimetableClasses(); //clasele la care e clientul

        for (TimetableClass t : tcTrainer)
            if (tcClient.contains(t))
            {
                tcClient.remove(t);
                t.getUsers().remove(client);
                t.decrement();
            }

        trainer.setTimetableClasses(tcTrainer);
        client.setTimetableClasses(tcClient);

        userDao.save(trainer);
        userDao.save(client);
    }

    /**
     * Adaga o clasa de fitness de pe orar unui client, in urma abonarii acestuia
     * @param user
     * @param tc clasa de fitness pe care vrem sa o adaugam clientului
     */
    public void addUserTC(User user, TimetableClass tc)
    {
        if (user.getTimetableClasses()==null)
        {
            List<TimetableClass> listTC = new ArrayList<>();
            listTC.add(tc);
            user.setTimetableClasses(listTC);
            userDao.save(user);
        }
        else {
            user.getTimetableClasses().add(tc);
            userDao.save(user);
        }

        tc.addUser(user);
        timetableClassDao.save(tc);
    }


}
