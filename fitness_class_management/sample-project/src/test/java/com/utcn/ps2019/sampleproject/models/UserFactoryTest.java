package com.utcn.ps2019.sampleproject.models;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UserFactoryTest {

    @Test(expected = RoleTypeException.class)
    public void createUserByRole1() throws RoleTypeException {
        //thrown.expect(RoleTypeException.class);
        UserFactory userFactory = new UserFactory();
        Trainer trainer = (Trainer)userFactory.createUserByRole("ROLE_TRAINDDDDR");
        Client client = (Client)userFactory.createUserByRole("ROLE_CLIENT");
    }

    @Test(expected = Test.None.class)
    public void createUserByRole2() throws RoleTypeException {
        //thrown.expect(RoleTypeException.class);
        UserFactory userFactory = new UserFactory();
        Trainer trainer = (Trainer)userFactory.createUserByRole("ROLE_TRAINER");
        Client client = (Client)userFactory.createUserByRole("ROLE_CLIENT");
    }

    @Test(expected = Test.None.class)
    public void raport() throws RoleTypeException
    {
        UserFactory userFactory = new UserFactory();
        FClass fClass = new FClass("Zumba",StrengthType.EASY);
        TimetableClass tc1 = new TimetableClass(LocalDateTime.of(2019,03,28,15,30),fClass,10);
        TimetableClass tc2 = new TimetableClass(LocalDateTime.of(2019,03,29,16,30),fClass,10);

        List<TimetableClass> classes = new ArrayList<>();
        classes.add(tc1);
        classes.add(tc2);

        Trainer trainer = (Trainer)userFactory.createUserByRole("ROLE_TRAINER");
        Client client = (Client)userFactory.createUserByRole("ROLE_CLIENT");
        client.setUsername("popana");

        client.setTimetableClasses(classes);
        trainer.setTimetableClasses(classes);
        tc1.setUsers(Arrays.asList(client,trainer));
        tc2.setUsers(Arrays.asList(client,trainer));

        List<Client> trainerRaport = trainer.raport();
        List<TimetableClass> clientRaport = client.raport();

        assertEquals(trainerRaport.get(0).getUsername(),"popana");
        assertEquals(clientRaport.get(0).getDate(),LocalDateTime.of(2019,03,28,15,30));
    }
}