package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.FClassDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.StrengthType;
import com.utcn.ps2019.sampleproject.models.Style;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FClassServiceTest {
    @Autowired
    private FClassService fClassService;

    @MockBean
    private FClassDao fClassDao;

    FClass fClass2;

    @Before
    public void setup()
    {
        Style style1 = new Style("Restorative");
        Style style2 = new Style("Active");

        List<FClass> classes = new ArrayList<>();
        FClass fClass1 = new FClass("Zumba", StrengthType.EASY);
        fClass2 = new FClass("Pilates", StrengthType.MEDIUM);
        fClass1.setStyle(style1);
        fClass2.setStyle(style2);

        classes.add(fClass1);
        classes.add(fClass2);

        Mockito.when(fClassDao.findAll())
                .thenReturn(classes);

        when(fClassDao.findById(1)).thenReturn(Optional.of(fClass2));
    }


    @Test
    public void getClassesByStyle() {
        List<FClass> classes = fClassService.getClassesByStyle("Restorative");
        verify(fClassDao).findAll();
        assertEquals(classes.get(0).getName(),"Zumba");
    }

    @Test
    public void createClass()
    {
        FClass fClass = new FClass("dance aerobic", StrengthType.EASY);
        fClassService.createClass(fClass);
        verify(fClassDao).save(fClass);
    }

    @Test
    public void deleteClass()
    {
        FClass fc = new FClass();
        fClassService.deleteClass(fc);
        verify(fClassDao).delete(fc);
    }


    @Test
    public void update()
    {
        FClass fClass = new FClass("Tae Bo", StrengthType.HARD);

        fClassService.update(1,fClass);

        verify(fClassDao).save(fClass2);
        verify(fClassDao).findById(1);
        assertEquals(fClass2.getName(),"Tae Bo");
    }
}