package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.RoleDao;
import com.utcn.ps2019.sampleproject.models.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleServiceTest {

    @Autowired
    private RoleService roleService;

    @MockBean
    private RoleDao roleDao;

    Role role;

    @Before
    public void setup()
    {
        role = new Role("ROLE_TRAINER");

        when(roleDao.findById(1)).thenReturn(Optional.of(role));
    }

    @Test
    public void createRole() {
        Role role1 = new Role("ROLE_CLIENT");
        roleService.createRole(role1);
        verify(roleDao).save(role1);
    }

    @Test
    public void deleteRole() {
        Role r = new Role();
        roleService.deleteRole(r);
        verify(roleDao).delete(r);
    }

    @Test
    public void update() {
        Role newRole = new Role("ROLE_CLIENT");

        roleService.update(1,newRole);

        verify(roleDao).save(role);
        verify(roleDao).findById(1);
        assertEquals(role.getName(),"ROLE_CLIENT");
    }
}