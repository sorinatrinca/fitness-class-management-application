package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.StyleDao;
import com.utcn.ps2019.sampleproject.models.Style;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StyleServiceTest {
    @Autowired
    private StyleService styleService;

    @MockBean
    private StyleDao styleDao;

    Style style2;

    @Before
    public void setup()
    {
        style2 = new Style("Active");

        Mockito.when(styleDao.findById(1)).thenReturn(Optional.of(style2));
    }
    @Test
    public void createStyle() {
        Style style = new Style("Restorative");
        styleService.createStyle(style);
        verify(styleDao).save(style);
    }

    @Test
    public void deleteStyle() {
        Style style = new Style();
        styleService.deleteStyle(style);
        verify(styleDao).delete(style);
    }

    @Test
    public void update() {
        Style style = new Style("Activee");

        styleService.update(1,style);

        verify(styleDao).save(style2);
        verify(styleDao).findById(1);
        assertEquals(style2.getName(),"Activee");
    }
}