package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.TimetableClassDao;
import com.utcn.ps2019.sampleproject.models.FClass;
import com.utcn.ps2019.sampleproject.models.StrengthType;
import com.utcn.ps2019.sampleproject.models.TimetableClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TimetableClassServiceTest {
    @Autowired
    private TimetableClassService timetableClassService;

    @MockBean
    private TimetableClassDao timetableClassDao;

    TimetableClass tc1;

    @Before
    public void setup()
    {
        FClass fClass1 = new FClass("Zumba", StrengthType.MEDIUM);
        FClass fClass2 = new FClass("Tae Bo", StrengthType.HARD);

        tc1 = new TimetableClass(LocalDateTime.of(2019,1,1,20,0),fClass1,10);
        TimetableClass tc2 = new TimetableClass(LocalDateTime.of(2019,1,1,15,30),fClass2,10);
        List<TimetableClass> tcs = new ArrayList<>();
        tcs.add(tc1);
        tcs.add(tc2);
        Mockito.when(timetableClassDao.findAll())
                .thenReturn(tcs);

        when(timetableClassDao.findById(1)).thenReturn(Optional.of(tc1));
    }

    @Test
    public void getTimetableClassesByType()
    {
        List<TimetableClass> tcs = timetableClassService.getTimetableClassesByType("Zumba");
        verify(timetableClassDao).findAll();
        assertEquals(tcs.get(0).getfClass().getName(),"Zumba");
        assertEquals(tcs.size(),1);
    }

    @Test
    public void createTimetableClass()
    {
        TimetableClass tc = new TimetableClass();
        timetableClassService.createTimetableClass(tc);
        verify(timetableClassDao).save(tc);
    }

    @Test
    public void deleteTimetableClass(){
        TimetableClass tc = new TimetableClass();
        timetableClassService.deleteTimetableClass(tc);
        verify(timetableClassDao).delete(tc);
    }

    @Test
    public void update()
    {
        FClass fClass2 = new FClass("Tae Bo", StrengthType.HARD);

        TimetableClass tc11 = new TimetableClass(1,LocalDateTime.of(2019,1,1,20,0),fClass2,10);
        timetableClassService.update(1,tc11);

        verify(timetableClassDao).save(tc1);
        verify(timetableClassDao).findById(1);
        assertEquals(tc11.getfClass().getName(),"Tae Bo");
    }

}