package com.utcn.ps2019.sampleproject.service;

import com.utcn.ps2019.sampleproject.data.UserDao;
import com.utcn.ps2019.sampleproject.models.Role;
import com.utcn.ps2019.sampleproject.models.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @MockBean
    private UserDao userDao;

    User user3;

    @Before
    public void setup()
    {
        User user1 = new User("Pop","Meda","pmeda","abcdef","pop.meda@yahoo.com");
        User user2 = new User("Radu","Andrei","randrei","23456","radu@gmail.com");
        user3 = new User(1,"Man","Iulian","mann","23456","mann@gmail.com");

        Role role1 = new Role("ROLE_TRAINER");
        Role role2 = new Role("ROLE_ADMIN");
        Role role3 = new Role("ROLE_CLIENT");

        user1.setRole(role1);
        user2.setRole(role2);
        user3.setRole(role3);

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);

        when(userDao.findAll())
                .thenReturn(users);
        when(userDao.findById(1)).thenReturn(Optional.of(user3));
    }

    @Test
    public void findAllTrainers() {
        List<User> expectedUsers = userService.findAllTrainers();
        verify(userDao).findAll();
        assertEquals(expectedUsers.get(0).getUsername(),"pmeda");
    }

    @Test
    public void findAllClients() {
        List<User> expectedUsers = userService.findAllClients();
        verify(userDao).findAll();
        assertEquals(expectedUsers.get(0).getUsername(),"mann");
    }

    @Test
    public void update(){
        User user = new User(1,"Mannn","Iulian","mann","23456","mann@gmail.com");
        userService.update(1,user);
        verify(userDao).save(user3);
        verify(userDao).findById(1);
        assertEquals(user3.getFirstName(),"Mannn");
    }
}